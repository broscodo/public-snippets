/* BroskiNetworks.com */
/* Last Modified: 11:59 AM 4/7/2016 */
/* 
 Desc: Gets the position and angles of you
 Usage: 
    Console: getmypos <name> 
*/
Pos_Data = {};
concommand.Add("getmypos",function(ply,cmd,args)
        local name = args[1];
        if name == nil then ply:ChatPrint("Blank Name"); return ;end
        local temp = {};
        temp = {Name = name, Position = ply:GetPos(), Angle = ply:GetAngles()};
        table.insert(Pos_Data,temp);
        ply:ChatPrint("Added position!");
end);
/*
 Desc: Gets the position and angles of you and prints it nicely
 Usage: 
    Console: print_pos 
*/
concommand.Add("print_pos",function(ply,cmd,args)

	    local Position = ply:GetPos();
	    local Ang = ply:GetAngles();
		ply:ChatPrint("{Position = Vector(" .. math.Round(Position.x) .. ", " .. math.Round(Position.y) .. ", " .. math.Round(Position.z) .. "), Angle = Angle(" .. math.Round(Ang.p) .. ", " .. math.Round(Ang.y) .. ", " .. math.Round(Ang.r) .. ")},\n")
        ply:ChatPrint("Printed Postion");
end);
/*  
    Desc: Gets the position and angles of the entity you are looking at.
    Usage:
        Console: geteyepos <name> 
*/
concommand.Add("geteyepos",function(ply,cmd,args)
        local name = args[1];
        if name == nil then ply:ChatPrint("Blank Name"); return ;end
        local temp = {};
        temp = {Name = name, Position = ply:GetEyeTrace().Entity:GetPos(), Angle = ply:GetEyeTrace().Entity:GetAngles(), Model = ply:GetEyeTrace().Entity:GetModel()};
        table.insert(Pos_Data,temp);
        ply:ChatPrint("Added position!");
end);

/*  
    Desc: Saves Positions to file <file>.txt
    Usage:
        Console: savetable <filename> 
*/
concommand.Add("savetable",function(ply,cmd,args)
        local name = args[1];
        if name == nil then ply:ChatPrint("Blank Name"); return ;end
        file.Write(name .. ".txt", util.TableToJSON(Pos_Data));
        ply:ChatPrint("Saved");

end);

/*  
    Desc: Clears the table
    Usage:
        Console: cleartable
*/
concommand.Add("cleartable",function(ply,cmd,args)
        Pos_Data = {};
end);
